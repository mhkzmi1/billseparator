<?php

namespace Database\Seeders;

use App\Models\Invoice;
use App\Models\Share;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class InvoiceSeeder extends Seeder
{

    /**
     * @var Invoice
     */
    private $invoiceModel;

    public function __construct(Invoice $invoiceModel)
    {

        $this->invoiceModel = $invoiceModel;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shares = Share::query()->get();

        $invoices = [];

        foreach ($shares as $share) {
            $invoices[] = [
                'share_id' => $share->id,
                'transaction_id' => $share->user->transactions()->inRandomOrder()->pluck('id')->first(),
                'user_id' => $share->user_id,
                'status' => Arr::random(['successful', 'failed']),
            ];
        }

        $this->invoiceModel->query()->insertOrIgnore($invoices);
    }
}
