<?php

namespace Database\Seeders;

use App\Models\Bill;
use App\Models\User;
use Illuminate\Database\Seeder;

class BillUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::query()->get();

        foreach ($users as $user) {
            $user->bills()->sync(
                Bill::query()
                    ->inRandomOrder()
                    ->take(rand(2, 5))
                    ->pluck('id')
            );
        }
    }
}
