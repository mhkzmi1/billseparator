<?php

namespace Database\Seeders;

use App\Models\PaymentType;
use App\Models\Status;
use Illuminate\Database\Seeder;

class PaymentTypeSeeder extends Seeder
{

    /**
     * @var Status $paymentTypeModel
     */
    private $paymentTypeModel;

    public function __construct(PaymentType $paymentTypeModel)
    {
        $this->paymentTypeModel = $paymentTypeModel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [];

        $j = 0;
        foreach (['cash', 'wallet', 'transfer', 'credit'] as $status) {
            $statuses[$j]['slug'] = $status;
            $statuses[$j]['created_at'] = now();
            $statuses[$j]['updated_at'] = now();
            $j++;
        }

        $this->paymentTypeModel
            ->query()
            ->insertOrIgnore($statuses);
    }
}
