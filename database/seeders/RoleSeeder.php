<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{

    /**
     * @var Role $roleModel
     */
    private $roleModel;

    public function __construct(Role $roleModel)
    {
        $this->roleModel = $roleModel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = [
            [
                'slug' => 'admin'
            ],
            [
                'slug' => 'customer'
            ]
        ];

        $this->roleModel
            ->query()
            ->insertOrIgnore($roles);

        $users = User::query()->get();

        foreach ($users as $user) {
            $user->roles()->sync(
                Role::query()
                    ->inRandomOrder()
                    ->take(rand(1, 2))
                    ->pluck('id')
            );
        }
    }
}
