<?php

namespace Database\Seeders;

use App\Models\Item;
use App\Models\Score;
use Illuminate\Database\Seeder;

class ScoreSeeder extends Seeder
{

    /**
     * @var Score
     */
    private $scoreModel;

    public function __construct(Score $scoreModel)
    {
        $this->scoreModel = $scoreModel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $itemsHaveScore = Item::query()->whereNotNull('shop_title')->get();

        $scores = [];

        $itemsHaveScore->each(function ($item) use (&$scores) {
            $scores[] = [
                'user_id' => $item->payer_id,
                'bill_id' => $item->bill_id,
                'item_id' => $item->id,
                'count' => rand(0,10) * 50,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        });

        $this->scoreModel->query()->insert($scores);

    }
}
