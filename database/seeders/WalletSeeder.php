<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class WalletSeeder extends Seeder
{

    /**
     * @var Wallet $walletModel
     */
    private $walletModel;

    public function __construct(Wallet $walletModel)
    {
        $this->walletModel = $walletModel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::query()->get();

        $wallets = [];

        foreach ($users as $user) {
            $sheba = '';
            for ($i = 0; $i < 25; $i++) {
                $sheba .= rand(0, 9);
            }
            $wallets[] = [
                'user_id' => $user->id,
                'card_number' => $user->transactions()->inRandomOrder()->select('card_number')->pluck('card_number')->first(),
                'sheba_number' => $sheba,
                'status' => Arr::random(['enabled', 'disabled']),
            ];
        }

        $this->walletModel->query()->insertOrIgnore($wallets);
    }
}
