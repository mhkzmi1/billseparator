<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{

    /**
     * @var Status $statusModel
     */
    private $statusModel;

    public function __construct(Status $statusModel)
    {
        $this->statusModel = $statusModel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [];

        $j = 0;
        foreach (['open', 'closed', 'settled'] as $status) {
            $statuses[$j]['slug'] = $status;
            $statuses[$j]['created_at'] = now();
            $statuses[$j]['updated_at'] = now();
            $j++;
        }

        $this->statusModel
            ->query()
            ->insertOrIgnore($statuses);
    }
}
