<?php

namespace Database\Seeders;

use App\Models\Bill;
use App\Models\City;;
use App\Models\Item;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        City::factory(50)->create();
        $this->call(TypeSeeder::class);
        $this->call(StatusSeeder::class);
        User::factory(60)->create();
        Bill::factory(60)->create();
        $this->call(BillUserSeeder::class);
        Item::factory(80)->create();
        $this->call(PaymentTypeSeeder::class);
        Transaction::factory(80)->create();
        $this->call(ShareSeeder::class);
        $this->call(InvoiceSeeder::class);
        $this->call(WalletSeeder::class);
        $this->call(ScoreSeeder::class);
        $this->call(RoleSeeder::class);
    }
}
