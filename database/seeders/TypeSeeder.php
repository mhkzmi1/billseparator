<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{

    /**
     * @var Type $typeModel
     */
    private $typeModel;

    public function __construct(Type $typeModel)
    {
        $this->typeModel = $typeModel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [];

        for ($i = 1; $i < 41; $i++) {
            $types []['slug'] = 'Type' . $i;
        }

        $this->typeModel
            ->query()
            ->insertOrIgnore($types);
    }
}
