<?php

namespace Database\Seeders;

use App\Models\Bill;
use App\Models\PaymentType;
use App\Models\Share;
use Illuminate\Database\Seeder;

class ShareSeeder extends Seeder
{

    /**
     * @var Share
     */
    private $shareModel;

    public function __construct(Share $shareModel)
    {
        $this->shareModel = $shareModel;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bills = Bill::query()->get();

        $shares = [];

        foreach ($bills as $bill) {

            $totalPrice = 0;

            $users = $bill->users()->get();
            $priceList = $bill->items->pluck('price')->toArray();
            $persons = $bill->users->count();

            foreach ($priceList as $item) {
                $totalPrice += $item;
            }
            foreach ($users as $user) {
                $shares[] = [
                    'bill_id' => $bill->id,
                    'user_id' => $user->id,
                    'balance' => $totalPrice / ($persons ?? 1), #Handle division by zero
                    'payment_type_id' => PaymentType::query()->inRandomOrder()->pluck('id')->first(),
                ];
            }
        }

        $this->shareModel->query()->insert($shares);

    }
}
