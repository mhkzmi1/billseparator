<?php

namespace Database\Factories;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'card_number' => $this->faker->randomElement([
                    '6274-12',
                    '6273-81',
                    '5057-85',
                    '6367-95',
                    '6391-94',
                    '6393-47',
                    '6277-60',
                    '6363-14',
                    '6273-53',
                    '5029-08',
                    '6276-48',
                    '6369-49',
                    '5029-38',
                    '5894-63',
                    '6219-86',
                    '5892-10',
                    '6396-07',
                    '6393-46',
                    '6037-69',
                    '6279-61',
                    '6063-73',
                    '6274-88',
                    '6392-17',
                    '5054-16',
                    '6280-23',
                    '9919-75',
                    '6037-99',
                    '6281-57',
                    '5058-01',
                    '6393-70',
                    '6395-99',
                ]) . rand(0, 9) . rand(0, 9) . '-' .
                rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . '-' .
                rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9),
            'bank_id' => rand(1, 32),
            'user_id' => User::query()->inRandomOrder()->pluck('id')->first(),
            'status' => $this->faker->randomElement(['successful', 'failed', 'successful', 'successful']),
            'track_code' => time(),
            'amount' => rand(1, 10) * 15000,
        ];


    }
}
