<?php

namespace Database\Factories;

use App\Models\Bill;
use App\Models\Status;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class BillFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bill::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->text(10),
            'status_id' => Status::query()->select('id')->inRandomOrder()->first(),
            'start_date' => $start_date = $this->faker->dateTimeBetween('-1 months', 'now'),
            'due_date' => Carbon::make($start_date)->addDays(rand(5, 10)),
        ];
    }
}
