<?php

namespace Database\Factories;

use App\Models\Bill;
use App\Models\Item;
use App\Models\Type;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        return [
            'bill_id' => $bill_id = Bill::query()->inRandomOrder()->pluck('id')->first(),
            'title' => $this->faker->text(15),
            'shop_title' => $shop_title = $this->faker->randomElement([null, $this->faker->name]),
            'shop_lat' => $shop_title === null ? null : $this->faker->latitude,
            'shop_long' => $shop_title === null ? null : $this->faker->longitude,
            'price' => $this->faker->randomNumber(1, 10) * $this->faker->randomElement([1000, 10000]),
            'payer_id' => Bill::query()->find($bill_id)->users->pluck('id')->first(),
            'type_id' => Type::query()->inRandomOrder()->select('id')->pluck('id')->first(),
        ];

    }

}
