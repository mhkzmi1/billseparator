<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * Items are subsets of bills. in fact each bill contains some of items.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bill_id');
            $table->string('title');
            $table->string('shop_title')->nullable();
            $table->decimal('shop_lat', 10, 8)->nullable();
            $table->decimal('shop_long', 11, 8)->nullable();
            $table->unsignedBigInteger('price');
            $table->unsignedInteger('payer_id')->nullable();
            $table->unsignedInteger('type_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            //Foreign Keys
            $table->foreign('bill_id')
                ->references('id')
                ->on('bills')
                ->onDelete('cascade');

            $table->foreign('payer_id')
                ->references('id')
                ->on('users');

            $table->foreign('type_id')
                ->references('id')
                ->on('types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
