<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique()->nullable();
            $table->string('phone_number')->unique()->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedBigInteger('club_total_score')->nullable();
            $table->unsignedBigInteger('club_spent_score')->nullable();
            $table->timestamp('birth_date')->nullable();
            $table->unsignedInteger('inviter_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            //Foreign Keys
            $table->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
