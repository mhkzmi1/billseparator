<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * Shares of persons from all bills will kept in this table.
     * Every bill with its all items has a share for involved persons in it.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shares', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('bill_id');
            $table->bigInteger('balance');
            $table->unsignedInteger('payment_type_id')->nullable();
            $table->timestamps();
            $table->softDeletes();


            //Foreign Keys
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('payment_type_id')
                ->references('id')
                ->on('payment_types')
                ->onDelete('set null');

            $table->foreign('bill_id')
                ->references('id')
                ->on('bills');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shares');
    }
}
