<?php


namespace App\Interfaces;


/**
 * Interface BillRepositoryInterface
 * @package App\Interfaces
 */
interface BillRepositoryInterface
{

    /**
     * @return mixed
     */
    public function getAllBills();

    /**
     * @param $bill_id
     * @return mixed
     */
    public function getBillById($bill_id);

    /**
     * @param array $bill
     * @return mixed
     */
    public function storeBill(array $bill);

    /**
     * @param $bill_id
     * @param array $bill_data
     * @return mixed
     */
    public function updateBill($bill_id, array $bill_data);

    /**
     * @param $bill_id
     * @return mixed
     */
    public function destroyBill($bill_id);

}
