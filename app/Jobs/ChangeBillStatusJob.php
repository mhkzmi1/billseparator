<?php

namespace App\Jobs;

use App\Interfaces\BillRepositoryInterface;
use App\Models\Bill;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ChangeBillStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var BillRepositoryInterface
     */
    private $billRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(BillRepositoryInterface $billRepository)
    {
        $this->billRepository = $billRepository;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        # It will find the bills which is open and their due date has been expired.
        # so they are not active and should be closed,
        # therefore their status changes to 'closed' mode.
        Bill::query()
            ->whereStatusId(1)
            ->whereDate('due_date', '<', now()->format('Y-m-d'))
            ->update([
                'status_id' => 2,
            ]);
    }
}
