<?php

namespace App\Providers;

use App\Interfaces\AuthRepositoryInterface;
use App\Interfaces\AuthServiceInterface;
use App\Interfaces\BillRepositoryInterface;
use App\Interfaces\BillServiceInterface;
use App\Interfaces\InvoiceRepositoryInterface;
use App\Interfaces\InvoiceServiceInterface;
use App\Interfaces\ItemRepositoryInterface;
use App\Interfaces\ItemServiceInterface;
use App\Interfaces\ScoreRepositoryInterface;
use App\Interfaces\ScoreServiceInterface;
use App\Interfaces\ShareRepositoryInterface;
use App\Interfaces\ShareServiceInterface;
use App\Interfaces\TransactionRepositoryInterface;
use App\Interfaces\TransactionServiceInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\UserServiceInterface;
use App\Interfaces\WalletRepositoryInterface;
use App\Interfaces\WalletServiceInterface;
use App\Repositories\AuthRepository;
use App\Repositories\BillRepository;
use App\Repositories\InvoiceRepository;
use App\Repositories\ItemRepository;
use App\Repositories\ScoreRepository;
use App\Repositories\ShareRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\UserRepository;
use App\Repositories\WalletRepository;
use App\Services\AuthService;
use App\Services\BillService;
use App\Services\InvoiceService;
use App\Services\ItemService;
use App\Services\ScoreService;
use App\Services\ShareService;
use App\Services\TransactionService;
use App\Services\UserService;
use App\Services\WalletService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        #Registering Bill to IoC Container
        $this->app->bind(BillRepositoryInterface::class, BillRepository::class);
        $this->app->bind(BillServiceInterface::class, BillService::class);

        #Registering Auth to IoC Container
        $this->app->bind(AuthRepositoryInterface::class, AuthRepository::class);
        $this->app->bind(AuthServiceInterface::class, AuthService::class);

        #Registering Invoice to IoC Container
        $this->app->bind(InvoiceRepositoryInterface::class, InvoiceRepository::class);
        $this->app->bind(InvoiceServiceInterface::class, InvoiceService::class);

        #Registering Item to IoC Container
        $this->app->bind(ItemRepositoryInterface::class, ItemRepository::class);
        $this->app->bind(ItemServiceInterface::class, ItemService::class);

        #Registering Score to IoC Container
        $this->app->bind(ScoreRepositoryInterface::class, ScoreRepository::class);
        $this->app->bind(ScoreServiceInterface::class, ScoreService::class);

        #Registering Share to IoC Container
        $this->app->bind(ShareRepositoryInterface::class,ShareRepository::class);
        $this->app->bind(ShareServiceInterface::class,ShareService::class);

        #Registering Transaction to IoC Container
        $this->app->bind(TransactionRepositoryInterface::class,TransactionRepository::class);
        $this->app->bind(TransactionServiceInterface::class,TransactionService::class);

        #Registering User to IoC Container
        $this->app->bind(UserRepositoryInterface::class,UserRepository::class);
        $this->app->bind(UserServiceInterface::class,UserService::class);

        #Registering Wallet to IoC Container
        $this->app->bind(WalletRepositoryInterface::class,WalletRepository::class);
        $this->app->bind(WalletServiceInterface::class,WalletService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
