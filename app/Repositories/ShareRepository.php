<?php


namespace App\Repositories;


use App\Interfaces\AuthRepositoryInterface;
use App\Models\Share;

/**
 * Class ShareRepository
 * @package App\Repositories
 */
class ShareRepository implements AuthRepositoryInterface
{
    /**
     * @var Share $shareModel
     */
    private $shareModel;

    public function __construct(Share $shareModel)
    {
        $this->shareModel = $shareModel;
    }

}
