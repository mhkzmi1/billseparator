<?php


namespace App\Repositories;


use App\Interfaces\AuthRepositoryInterface;
use App\Models\Score;

/**
 * Class ScoreRepository
 * @package App\Repositories
 */
class ScoreRepository implements AuthRepositoryInterface
{
    /**
     * @var Score $scoreModel
     */
    private $scoreModel;

    public function __construct(Score $scoreModel)
    {
        $this->scoreModel = $scoreModel;
    }

}
