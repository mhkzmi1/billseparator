Repositories are actually the lowest level of the implemented pattern in this application which deal with models.
In fact there is a lower level than a repository and that's model which contact with the database directly.

For each Entities in the application that need to contact with its model and eventually with the database we define a repository.

In a repository just the connection with model will establish, nothing else.
