<?php


namespace App\Repositories;


use App\Interfaces\BillRepositoryInterface;
use App\Models\Bill;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class BillRepository
 * @package App\Repositories
 */
class BillRepository implements BillRepositoryInterface
{
    /**
     * @var Bill $billModel
     */
    private $billModel;

    public function __construct(Bill $billModel)
    {
        $this->billModel = $billModel;
    }

    /**
     * Returns all Bills without pagination.
     * It would be better if the pagination handled in the controller layer.
     * @return false|Builder[]
     */
    public function getAllBills()
    {
        try {
            return $this->billModel->query()->get();
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Returns a Bill with its identification.
     * @return false|Builder[]
     */
    public function getBillById($bill_id)
    {
        try {
            return $this->billModel->query()->findOrFail($bill_id);
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Creates a new Bill with default status = OPEN.
     * @param array $bill
     * @return false|Builder[]
     * @throws \Throwable
     */
    public function storeBill(array $bill)
    {
        try {
            return DB::transaction(function () use ($bill) {
                $bill = $this->billModel->query()->create($bill);
                auth()->user()->bills()->sync($bill->id);
                return $bill;
            });
        } catch (\Throwable $exception) {
            return false;
        }
    }

    /**
     * Updates a new Bill.
     * @param array $bill
     * @return false|Builder[]
     * @throws \Throwable
     */
    public function updateBill($bill_id, array $bill_data)
    {
        try {
            return DB::transaction(function () use ($bill_id, $bill_data) {
                $bill = $this->billModel->query()
                    ->findOrFail($bill_id)
                    ->update($bill_data);
                auth()->user()->bills()->sync($bill_id);
                return $bill;
            });
        } catch (\Throwable $exception) {
            return false;
        }
    }

    /**
     * Deletes a Bill by the given id.
     * @param array $bill
     * @return false|Builder[]
     */
    public function destroyBill($bill_id)
    {
        try {
            return $this->billModel->query()
                ->findOrFail($bill_id)
                ->delete();
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Returns All bills which are belong to the authenticated user.
     * @param array $bill
     * @return false|Builder[]
     */
    public function getMyBills()
    {
        try {
            return auth()->user()->bills()->get();
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Returns a bill by id if it's in user's bills list.
     * @param array $bill
     * @return false|Builder[]
     */
    public function showMyBill($bill_id)
    {
        try {
            return auth()->user()
                ->bills()
                ->with('items')
                ->findOrFail($bill_id);
        } catch (\Exception $exception) {
            return false;
        }
    }
}
