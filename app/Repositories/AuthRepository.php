<?php


namespace App\Repositories;


use App\Interfaces\AuthRepositoryInterface;
use App\Models\User;

/**
 * Class AuthRepository
 * @package App\Repositories
 */
class AuthRepository implements AuthRepositoryInterface
{
    /**
     * @var User $userModel
     */
    private $userModel;

    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

}
