<?php


namespace App\Repositories;


use App\Interfaces\AuthRepositoryInterface;
use App\Models\User;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository implements AuthRepositoryInterface
{
    /**
     * @var User $userModel
     */
    private $userModel;

    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

}
