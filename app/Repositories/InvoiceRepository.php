<?php


namespace App\Repositories;


use App\Interfaces\AuthRepositoryInterface;
use App\Models\Invoice;

/**
 * Class InvoiceRepository
 * @package App\Repositories
 */
class InvoiceRepository implements AuthRepositoryInterface
{
    /**
     * @var Invoice $invoiceModel
     */
    private $invoiceModel;

    public function __construct(Invoice $invoiceModel)
    {
        $this->invoiceModel = $invoiceModel;
    }

}
