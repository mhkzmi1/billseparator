<?php


namespace App\Repositories;


use App\Interfaces\AuthRepositoryInterface;
use App\Models\Item;

/**
 * Class ItemRepository
 * @package App\Repositories
 */
class ItemRepository implements AuthRepositoryInterface
{
    /**
     * @var Item $itemModel
     */
    private $itemModel;

    public function __construct(Item $itemModel)
    {
        $this->itemModel = $itemModel;
    }

}
