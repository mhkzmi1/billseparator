<?php


namespace App\Repositories;


use App\Interfaces\AuthRepositoryInterface;
use App\Models\Transaction;

/**
 * Class TransactionRepository
 * @package App\Repositories
 */
class TransactionRepository implements AuthRepositoryInterface
{
    /**
     * @var Transaction $transactionModel
     */
    private $transactionModel;

    public function __construct(Transaction $transactionModel)
    {
        $this->transactionModel = $transactionModel;
    }

}
