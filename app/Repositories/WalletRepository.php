<?php


namespace App\Repositories;


use App\Interfaces\AuthRepositoryInterface;
use App\Models\Wallet;

/**
 * Class WalletRepository
 * @package App\Repositories
 */
class WalletRepository implements AuthRepositoryInterface
{
    /**
     * @var Wallet $walletModel
     */
    private $walletModel;

    public function __construct(Wallet $walletModel)
    {
        $this->walletModel = $walletModel;
    }

}
