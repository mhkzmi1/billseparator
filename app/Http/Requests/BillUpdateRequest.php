<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BillUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'string',
            'start_date' => 'date',
            'to_date' => 'date|after_or_equal:from_date',
        ];
    }

    public function data()
    {
        return $this->only('title', 'start_date', 'due_date');
    }
}
