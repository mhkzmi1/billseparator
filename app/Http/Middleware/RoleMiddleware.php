<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param array $roles
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if (Auth::guest() || !Auth::user()->hasAnyRole($roles)) {
            return response([
                'success' => false,
                'message' => 'Unauthorized Action',
            ], Response::HTTP_FORBIDDEN);
        }
        return $next($request);
    }
}
