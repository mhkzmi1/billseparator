<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Bill Separator",
     *      description="Bill Separator Demo Documentation",
     * )
     *
     * @OA\Tag(
     *     name="Bill Separator",
     *     description="API Endpoints of the Project"
     * )
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
