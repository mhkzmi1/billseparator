<?php

namespace App\Http\Controllers\Auth\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     * @OA\Post (
     *      path="/api/register",
     *      operationId="register",
     *      tags={"Auth"},
     *      summary="User registration",
     *      description="Register a new user",
     *
     * @OA\Parameter(
     *      name="name",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     * @OA\Parameter(
     *      name="phone_number",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="number"
     *      )
     *  ),
     * @OA\Parameter(
     *      name="email",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string",
     *          format="email"
     *      )
     *  ),
     * @OA\Parameter(
     *      name="password",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string",
     *          format="password"
     *      )
     *  ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function register(Request $request): Response
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone_number' => 'required|numeric|regex:/[989]\d{9}/|unique:users',
            'username' => 'string|max:20|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'inviter_id' => 'numeric|numeric|max:5000'
        ]);

        $request['password'] = Hash::make($request->input('password'));
        $request['remember_token'] = Str::random(10);

        try {
            return DB::transaction(function () use ($request) {

                $user = User::create($request->toArray());

                # Adding default role operator to user
                $user->roles()->sync([2]);

                $token = $user->createToken('Laravel Password Grant Client')->accessToken;

                return response([
                    'success' => true,
                    'user' => $user,
                    'access_token' => $token
                ], Response::HTTP_OK);
            });

        } catch (\Exception $exception) {
            return response([
                'success' => false,
                'message' => trans('auth.failed-registration')
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }


    /**
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function login(Request $request): Response
    {
        $this->validate($request, [
            'username' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        $user = User::where('email', $request->username)->first();

        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                return response([
                    'success' => true,
                    'access_token' => $token
                ], Response::HTTP_OK);
            } else {
                return response([
                    'success' => false,
                    'message' => trans('auth.password')
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {
            return response([
                'success' => false,
                "message" => trans('auth.failed-login')
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @OA\Post (
     *      path="/api/logout",
     *      operationId="logout",
     *      tags={"Auth"},
     *      summary="Log out of the system",
     *      description="Revokes the user's token",
     *      security={{"myAuth": {}},
     *   },
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      )
     *     )
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request): Response
    {
        $request->user()->token()->revoke();
        return response([
            'success' => true,
            'message' => trans('auth.logout')
        ], Response::HTTP_OK);
    }
}
