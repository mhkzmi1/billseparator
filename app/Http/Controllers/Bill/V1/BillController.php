<?php

namespace App\Http\Controllers\Bill\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\BillStoreRequest;
use App\Http\Requests\BillUpdateRequest;
use App\Interfaces\BillServiceInterface;
use Illuminate\Http\Response;

class BillController extends Controller
{

    /**
     * @var BillServiceInterface $billService
     */
    private $billService;

    public function __construct(BillServiceInterface $billService)
    {
        $this->billService = $billService;
    }

    /**
     * @OA\Get (
     *      path="/api/v1/bills",
     *      operationId="getAllBills",
     *      tags={"Bill"},
     *      summary="Get All Bills",
     *      description="Display a listing of the bills",
     *      security={{"myAuth": {}}},
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     * @return Response
     */
    public function index(): Response
    {
        $bills = $this->billService->getAllBills();

        return response([
            'success' => false !== $bills,
            'message' => !$bills ? 'There is no bill yet!' : "List of Bills",
            'bills' => $bills ?: []
        ], Response::HTTP_OK);
    }

    /**
     * @OA\Post (
     *      path="/api/v1/bills",
     *      operationId="storeBill",
     *      tags={"Bill"},
     *      summary="Create a new Bill",
     *      description="Store a newly created Bill in storage.",
     *      security={{"myAuth": {}}},

     * @OA\Parameter(
     *      name="title",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     * @OA\Parameter(
     *      name="start_date",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *          type="string",
     *          format="date"
     *      )
     *  ),
     * @OA\Parameter(
     *      name="due_date",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *          type="string",
     *          format="date"
     *      )
     *  ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     *
     * @param BillStoreRequest $request
     * @return Response
     */
    public function store(BillStoreRequest $request): Response
    {
        $bill = $this->billService->storeBill(array_merge($request->data(), ['status_id' => '2']));

        return response([
            'success' => false !== $bill,
            'message' => !$bill ? 'Unsuccessful!' : "Bill has been created",
            'bill' => $bill ?: []
        ], Response::HTTP_OK);
    }

     /**
     * @OA\Get (
     *      path="/api/v1/bills/{id}",
     *      operationId="getBillById",
     *      tags={"Bill"},
     *      summary="Get a Bill by its ID",
     *      description="Display the specified resource.",
     *      security={{"myAuth": {}}},
     *
     * @OA\Parameter (
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $bill = $this->billService->getBillById($id);

        return response([
            'success' => false !== $bill,
            'message' => !$bill ? 'Bill Not Found' : "Bill $id",
            'bill' => $bill ?: []
        ], Response::HTTP_OK);
    }

    /**
     * @OA\Put   (
     *      path="/api/v1/bills/{id}",
     *      operationId="updateBill",
     *      tags={"Bill"},
     *      summary="Update a Bill",
     *      description="Update a specified Bill",
     *      security={{"myAuth": {}}},
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     * @OA\Parameter(
     *      name="title",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     * @OA\Parameter(
     *      name="start_date",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *          type="string",
     *          format="date"
     *      )
     *  ),
     * @OA\Parameter(
     *      name="due_date",
     *      in="query",
     *      required=false,
     *      @OA\Schema(
     *          type="string",
     *          format="date"
     *      )
     *  ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     *
     * @param BillUpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(BillUpdateRequest $request, int $id)
    {
        $bill = $this->billService->updateBill($id, $request->merge(['status_id' => '2'])->toArray());

        return response([
            'success' => false !== $bill,
            'message' => !$bill ? 'Unsuccessful!' : "Bill has been updated",
            'bill' => $bill ?: []
        ], Response::HTTP_OK);
    }

    /**
     * @OA\Delete (
     *      path="/api/v1/bills/{id}",
     *      operationId="deleteBill",
     *      tags={"Bill"},
     *      summary="Delete a Bill",
     *      description="SoftDelete the specified Bill from storage.",
     *      security={{"myAuth": {}}},
     *
     * @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     * @param int $id
     * @return Response
     */
    public function destroy(int $id): Response
    {
        $bill = $this->billService->destroyBill($id);

        return response([
            'success' => false !== $bill,
            'message' => !$bill ? 'Unsuccessful!' : "Bill $id has been removed",
            'bill' => []
        ], Response::HTTP_OK);
    }

    /**
     * @OA\Get (
     *      path="/api/v1/mine/bills",
     *      operationId="getAuthUserBills",
     *      tags={"Bill"},
     *      summary="Get auth user's bills",
     *      description="Get the authenticated user's bills list.",
     *      security={{"myAuth": {}}},
     *
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     *
     *
     * @return Response
     */
    public function getMyBills(): Response
    {
        $bills = $this->billService->getMyBills();

        return response([
            'success' => false !== $bills,
            'message' => !$bills ? 'Unsuccessful!' : "Authenticated user's bills",
            'bill' => $bills ?: []
        ], Response::HTTP_OK);
    }

    /**
     * @OA\Get (
     *      path="/api/v1/mine/bills/{id}",
     *      operationId="getMyBillById",
     *      tags={"Bill"},
     *      summary="Get a user's Bill by its ID",
     *      description="This functionality will return a bill by given id in case if it belongs to the current user.",
     *      security={{"myAuth": {}}},
     *
     * @OA\Parameter (
     *      name="id",
     *      in="path",
     *      required=true,
     *      @OA\Schema(
     *          type="string"
     *      )
     *  ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     * @param $id
     * @return Response
     */
    public function showMyBill($id): Response
    {
        $bill = $this->billService->showMyBill($id);

        return response([
            'success' => false !== $bill,
            'message' => !$bill ? "You haven't bill with id : $id" : "Authenticated user's bills",
            'bill' => $bill ?: []
        ], Response::HTTP_OK);
    }
}
