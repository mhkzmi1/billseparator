<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Bill
 *
 * @property int $id
 * @property string $title
 * @property int $status_id
 * @property string|null $start_date
 * @property string|null $due_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static Builder|Bill newModelQuery()
 * @method static Builder|Bill newQuery()
 * @method static Builder|Bill query()
 * @method static Builder|Bill whereCreatedAt($value)
 * @method static Builder|Bill whereDeletedAt($value)
 * @method static Builder|Bill whereDueDate($value)
 * @method static Builder|Bill whereId($value)
 * @method static Builder|Bill whereStartDate($value)
 * @method static Builder|Bill whereStatusId($value)
 * @method static Builder|Bill whereTitle($value)
 * @method static Builder|Bill whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Bill extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'status_id',
        'start_date',
        'due_date'
    ];

    protected $hidden = [
        'pivot'
    ];


    protected $appends = [
        'total_price'
    ];

    /*
    |--------------------------------------------------------------------------
    | Mutators
    |--------------------------------------------------------------------------
    */

    public function getTotalPriceAttribute()
    {
        $price = 0;
        $items = $this->items()->select('price')->pluck('price');
        foreach ($items as $item) {
            $price += $item;
        }
        return $price;
    }

    /*
    |--------------------------------------------------------------------------
    | Relations
    |--------------------------------------------------------------------------
    */
    public function status(): HasOne
    {
        return $this->hasOne(Status::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'bill_user');
    }

    public function scores(): HasMany
    {
        return $this->hasMany(Score::class, 'id', 'bill_id');
    }

    public function items(): HasMany
    {
        return $this->hasMany(Item::class);
    }

    public function shares(): BelongsTo
    {
        return $this->belongsTo(Share::class);
    }
}
