<?php


namespace App\Services;


use App\Interfaces\AuthRepositoryInterface;
use App\Interfaces\AuthServiceInterface;
use App\Repositories\AuthRepository;

/**
 * Class AuthService
 * @package App\Services
 */
class AuthService implements AuthServiceInterface
{

    /**
     * @var AuthRepository $authRepository
     */
    private $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

}
