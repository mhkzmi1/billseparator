<?php


namespace App\Services;


use App\Interfaces\ShareRepositoryInterface;
use App\Interfaces\ShareServiceInterface;
use App\Repositories\ShareRepository;

/**
 * Class ShareService
 * @package App\Services
 */
class ShareService implements ShareServiceInterface
{

    /**
     * @var ShareRepository $shareRepository
     */
    private $shareRepository;

    public function __construct(ShareRepositoryInterface $shareRepository)
    {
        $this->shareRepository = $shareRepository;
    }

}
