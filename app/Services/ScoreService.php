<?php


namespace App\Services;


use App\Interfaces\ScoreRepositoryInterface;
use App\Interfaces\ScoreServiceInterface;
use App\Repositories\ScoreRepository;

/**
 * Class ScoreService
 * @package App\Services
 */
class ScoreService implements ScoreServiceInterface
{

    /**
     * @var ScoreRepository $scoreRepository
     */
    private $scoreRepository;

    public function __construct(ScoreRepositoryInterface $scoreRepository)
    {
        $this->scoreRepository = $scoreRepository;
    }

}
