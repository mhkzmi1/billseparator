<?php


namespace App\Services;


use App\Interfaces\ItemRepositoryInterface;
use App\Interfaces\ItemServiceInterface;
use App\Repositories\ItemRepository;

/**
 * Class ItemService
 * @package App\Services
 */
class ItemService implements ItemServiceInterface
{

    /**
     * @var ItemRepository $itemRepository
     */
    private $itemRepository;

    public function __construct(ItemRepositoryInterface $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

}
