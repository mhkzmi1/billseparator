<?php


namespace App\Services;


use App\Interfaces\WalletRepositoryInterface;
use App\Interfaces\WalletServiceInterface;
use App\Repositories\WalletRepository;

/**
 * Class WalletService
 * @package App\Services
 */
class WalletService implements WalletServiceInterface
{

    /**
     * @var WalletRepository $walletRepository
     */
    private $walletRepository;

    public function __construct(WalletRepositoryInterface $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }

}
