<?php


namespace App\Services;


use App\Interfaces\InvoiceRepositoryInterface;
use App\Interfaces\InvoiceServiceInterface;
use App\Repositories\InvoiceRepository;

/**
 * Class InvoiceService
 * @package App\Services
 */
class InvoiceService implements InvoiceServiceInterface
{

    /**
     * @var InvoiceRepository $invoiceRepository
     */
    private $invoiceRepository;

    public function __construct(InvoiceRepositoryInterface $invoiceRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
    }

}
