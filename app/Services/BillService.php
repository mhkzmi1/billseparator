<?php


namespace App\Services;


use App\Interfaces\BillRepositoryInterface;
use App\Interfaces\BillServiceInterface;
use App\Repositories\BillRepository;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class BillService
 * @package App\Services
 */
class BillService implements BillServiceInterface
{

    /**
     * @var BillRepository $billRepository
     */
    private $billRepository;

    public function __construct(BillRepositoryInterface $billRepository)
    {
        $this->billRepository = $billRepository;
    }

    /**
     * @return false|Builder[]
     */
    public function getAllBills()
    {
        return $this->billRepository->getAllBills();
    }

    /**
     * @param $bill_id
     * @return false|Builder[]
     */
    public function getBillById($bill_id)
    {
        return $this->billRepository->getBillById($bill_id);
    }

    /**
     * @param array $bill
     * @return false|Builder[]
     * @throws \Throwable
     */
    public function storeBill(array $bill)
    {
        return $this->billRepository->storeBill($bill);
    }

    /**
     * @param $bill_id
     * @param array $bill_data
     * @return false|Builder[]
     * @throws \Throwable
     */
    public function updateBill($bill_id, array $bill_data)
    {
        return $this->billRepository->updateBill($bill_id, $bill_data);
    }

    /**
     * @param $bill_id
     * @return false|Builder[]
     */
    public function destroyBill($bill_id)
    {
        return $this->billRepository->destroyBill($bill_id);
    }

    /**
     * @return false|Builder[]
     */
    public function getMyBills()
    {
        return $this->billRepository->getMyBills();
    }

    /**
     * @param $bill_id
     * @return false|Builder[]
     */
    public function showMyBill($bill_id)
    {
        return $this->billRepository->showMyBill($bill_id);
    }
}
