## About The Project
### Bill Separator

If you spend lots of times with your friends and hang out, and also one person buys for all people in the group, it probably might be important that each person pay their share.

In this application we just want to manage all costs and calculate each portion.

Main concept of our idea are Bills, which contain couples of items. In fact everything we pay for it is an item and belongs to a bill.
Items have types (Ex. drinks, foods, entertainments, rents,...).

If user enter the type of the items or tell the name and location of the shop which has bought from, they will get points (scores).

There are only 2 types of users in this application, admin and customer and the permissioning system is so simple and use a flat role-user logic.

The Architecture of this project based on laravel,and **Service-Repository** pattern has implemented as the main design pattern.

Some short description of services and repositories are in below links:


### Installation

1. Just clone this app to your local system
> git clone https://gitlab.com/mhkzmi1/billseparator.git

2. Change your directory to the cloned repository.
> cd billseparator

3. Use composer in order to install the required dependencies.
> composer install

3. Create a database in your database server. in this project MySQL has been used.

4. Copy the .env.example file to a new one and name it .env , in order to set environment variables in it
   Then run php artisan key:generate
> cp .env.example .env && php artisan key:generate   

5. Puts your database user credentials and the database name which has created for this project in the DotEnv file.


6. Run this commands to create tables and seed them.
> php artisan migrate --seed

* note if the seeding got into problem please run this command again:
> php artisan migrate:fresh --seed

7. Now we need to generate keys for laravel passport and put clients credentials in the database
> php artisan passport:install

8. Finally we can serve our application by the command:
> php artisan serve
