<?php

use App\Http\Controllers\Auth\Api\AuthController;
use App\Http\Controllers\Bill\V1\BillController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Public Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['cors', 'json.response']], function () {

    Route::post('/login', [AuthController::class, 'login'])->name('login.api');
    Route::post('/register', [AuthController::class, 'register'])->name('register.api');

});


/*
|--------------------------------------------------------------------------
| Protected Routes
|--------------------------------------------------------------------------
*/
Route::middleware('auth:api')->group(function () {

    Route::post('/logout', [AuthController::class, 'logout'])->name('logout.api');


    ##########################################
    ## Admin Services ##
    ##########################################
    Route::middleware('role:admin')->prefix('v1')->group(function () {

        Route::resource('/bills', BillController::class)->only(['show', 'index']);


    });

    ##########################################
    ## Customer Services ##
    ##########################################
    Route::middleware('role:customer')->prefix('v1')->group(function () {

        Route::resource('/bills', BillController::class)->only(['store', 'update', 'destroy']);
        Route::get('/mine/bills', [BillController::class,'getMyBills'])->name('bills.mine.index');
        Route::get('/mine/bills/{id}', [BillController::class,'showMyBill'])->name('bills.mine.show');

    });

});


